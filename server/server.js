const express = require('express')
const path = require('path')
const app = express()
const publicPath = path.join(__dirname, '..', 'public')

// Heroku sets this
const port = process.env.PORT || 3000

app.use(express.static(publicPath))

// always just serve index.html
app.get('*', (req, resp) => {
    resp.sendFile(path.join(publicPath, 'index.html'))
})

app.listen(port, () => {
    console.log('Server is running')
})