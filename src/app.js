import React from 'react'
import ReactDOM from 'react-dom'
import TosserBoard from './components/TosserBoard'
import 'normalize.css/normalize.css'
import './styles/styles.scss'

ReactDOM.render(<TosserBoard />, document.getElementById('app'))