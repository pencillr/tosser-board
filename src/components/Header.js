import React from 'react'

const Header = (props) => {

    return (
        <div className="header">
            <h1 className="header__title">tosserboard</h1>
            <hr className="header__subtitle" />
            <hr className="header__hr" />
        </div>
    )
}

export default Header