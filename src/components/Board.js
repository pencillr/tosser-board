import React from 'react'
import ModalAdd from './ModalAdd'
import Tosser from './Tosser'
import Button from '@material/react-button';

export default class Board extends React.Component {
    constructor(props) {
        super(props)
        this.state =  {
            show: false,
            tossers: props.tossers
        }
        this.handleClose = this.handleClose.bind(this)
        this.handleOpen = this.handleOpen.bind(this)
        this.handleAddTosser = this.handleAddTosser.bind(this)
        this.handleRemoveAll = this.handleRemoveAll.bind(this)
        this.handleRemoveTosser = this.handleRemoveTosser.bind(this)
    }

    componentDidMount(){
        try {
            const json = localStorage.getItem('tossers')
            const tossers = JSON.parse(json)
            if (tossers) {
                this.setState(() => ({ tossers }))
            }
        } catch (e) {
            console.log(e)
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if (prevState.tossers.length !== this.state.tossers.length) {
            const json = JSON.stringify(this.state.tossers)
            localStorage.setItem('tossers', json)
            console.log('saving data')
        }
    }

    handleOpen() {
        this.setState(() => ({
            show: true
        }))
    }

    handleClose() {
        this.setState(() => ({
            show: false
        }))
    }

    handleRemoveAll() {
        const r = confirm("Do you want to clear all tossers?")
        if (r) {
            this.setState(() => ({
                tossers: []
            }))
        }
    }

    handleRemoveTosser(name) {
        const r = confirm(`Do you want to remove tosser ${name}?`)
        if (r) {
            this.setState((prevState) => {
                return {
                    tossers: prevState.tossers.filter(
                        tosser => tosser.name != name
                    )
                }
            })
        }
    }

    handleAddTosser(e) {
        e.preventDefault()
        const nums = e.target.elements.nums.value
        const dice = e.target.elements.dice.value
        const addition = e.target.elements.addition.value
        const name = e.target.elements.name.value.trim()
        if (name) {

            if (this.state.tossers.filter(
                tosser => tosser.name === name
            ).length) {
                alert("Name mut be unique!")
            } else {
                this.setState((prevState) => ({
                    tossers: prevState.tossers.concat([{
                        name: name,
                        nums: nums,
                        dice: dice,
                        addition: addition
                    }])
                }))
            }
        } else {
            alert("You must provide a name!")
        }
        this.handleClose()
    }

    render() {
        return (
            <div>
                <ModalAdd 
                    show={this.state.show} 
                    handleAddTosser={this.handleAddTosser}
                    handleClose={this.handleClose}/>
                <div className="flex-tosser-container">
                    {this.state.tossers.length === 0 && <div className="text-align"><p>no tosser added yet...</p></div>}
                    {
                        this.state.tossers.map(
                            (tosser) => <Tosser 
                                key={Math.random().toString(36).slice(2)}
                                name={tosser.name}
                                nums={tosser.nums}
                                dice={tosser.dice} 
                                addition={tosser.addition} 
                                handleRemoveTosser={this.handleRemoveTosser} />
                        )
                    }
                </div>
                <div className="flex-button-container">
                    <Button raised onClick={this.handleOpen} className="button-alternate">Add Tosser</Button>
                    {this.state.tossers.length != 0 && 
                        <Button raised onClick={this.handleRemoveAll} className="button-alternate">Clear</Button>
                    }
                </div>
            </div>
        )
    }
}

Board.defaultProps = {
    tossers: []
}