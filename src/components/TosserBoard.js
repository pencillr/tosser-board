import React from 'react'
import Header from './Header'
import Board from './Board'

export default class TosserBoard extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="app">
                <Header />
                <Board />
            </div>
        )
    }
}