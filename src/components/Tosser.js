import React from 'react'
import Button from '@material/react-button';

export default class Tosser extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            roll: undefined
        }
        this.tossEm = this.tossEm.bind(this)
        this.handleRemove =this.handleRemove.bind(this)
    }

    tossEm(e) {
        e.preventDefault()
        const num = e.target.elements.nums.value
        const dice = e.target.elements.dice.value
        let addition = 0
        if (e.target.elements.addition) {
            addition = e.target.elements.addition.value
        }
        let roll_result = []
        for (let step = 1; step <= num; step++) {
            const _result = (Math.floor(Math.random() * dice.slice(1)) + 1) + Number(addition)
            roll_result.push(_result.toString())
        }
        this.setState(() => ({
            roll: roll_result
        }))
        // console.log(Math.floor(Math.random() * dice.slice(1)) + 1)
    }

    handleRemove() {
        this.props.handleRemoveTosser(this.props.name)
    }

    render() {
        return (
            <div className="tosser">
                <div className="flex-headline-container">
                    <p className="title">{this.props.name}</p>
                    <button onClick={this.handleRemove}>×</button>
                </div>
                <div className="flex-tosser-container">
                    <form onSubmit={this.tossEm}>
                        <input type="number" id="nums" name="nums" defaultValue={this.props.nums} min="0" max="30" />
                        <span> x </span>
                        <select name="dice" id="dice" defaultValue={this.props.dice}>
                            <option value="k6">k6</option>
                            <option value="k10">k10</option>
                            <option value="k100">k100</option>
                            <option value="k20">k20</option>
                        </select>
                        {this.props.addition != 0 && <span> + </span>}
                        {this.props.addition != 0 && <input type="number" id="addition" name="addition" defaultValue={this.props.addition} min="0" max="30" />}
                        <Button raised className="button-alternate">Toss!</Button>
                    </form>
                    <div className="flex-result-container">
                        {this.state.roll && 
                            this.state.roll.map(
                                (result) => <div key={Math.random().toString(36).slice(2)} className="results">{result}</div>
                            )
                        }
                    </div>
                </div>
            </div>
        )
    }
}