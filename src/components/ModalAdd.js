import React from 'react'
import Modal from 'react-modal'
import Button from '@material/react-button';

Modal.setAppElement(document.getElementById('app'))

const ModalAdd = (props) => {
    return (
        <Modal
            isOpen={props.show}
            onRequestClose={props.handleClose}
            contentLabel="Create Tosser!"
            closeTimeoutMS={200}
            className="modal"
        >
            <h4>Add your tosser!</h4>
            <form onSubmit={props.handleAddTosser}>
                <input type="text" id="name" name="name" placeholder="Name"/>
                <hr />
                <input type="number" id="nums" name="nums" defaultValue="1" min="0" max="30" />
                <span> x </span>
                <select name="dice" id="dice">
                    <option value="k6" >k6</option>
                    <option value="k10">k10</option>
                    <option value="k100">k100</option>
                    <option value="k20">k20</option>
                </select>
                <span> + </span>
                <input type="number" id="addition" name="addition" defaultValue="0" min="0" max="30" />
                <Button raised className="button-alternate-modal">Add</Button>
            </form>
        </Modal>
    )
}

export default ModalAdd